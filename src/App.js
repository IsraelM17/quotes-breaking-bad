import React, { useState, useEffect } from 'react';
import styled from "@emotion/styled";
import logo from './logo.svg';
import './App.css';
import Frase from "./components/Frase";

const Boton = styled.button`
    background: -webkit-linear-gradient(top left, #007d35, #007d35 40%, #0f574e 100%);
    background-size: 30px;
    font-family: Arial, Helvetica, sans-serif;
    color: #fff;
    margin-top: 3rem;
    padding: 1rem 3rem;
    font-size: 2rem;
    border: 2px solid black;
    transition: background-size .8s ease;
    
    :hover{
        cursor: pointer;
        background-size: 400px;
    }
 `;

const Contenedor = styled.div`
    display: flex;
    align-items: center;
    padding-top: 5rem;
    flex-direction: column;
`;

function App() {

    //State de frases
    const [frase, obtenerFrase] = useState({});

    const consultarAPI = async () => {

        const api   = await fetch('https://www.breakingbadapi.com/api/quote/random');
        const frase = await api.json();

        obtenerFrase(frase[0]);

    };

    //useEfect Cargar una frase
    useEffect(() => { consultarAPI() },[]);

  return (
      <Contenedor>
          <Frase
              frase = { frase }
          />
        <Boton
            onClick = { consultarAPI }
        > Cargar Frase </Boton>
      </Contenedor>
  );
}

export default App;
